#include <catch/catch.hpp>
#include "Document.hh"

SCENARIO("Test WebPage") {
  const std::string string = "http://new_test_url.com";
  const std::string string_2 = "http://new_test_url.eu";
  // nx::Document cant_be_created_since_its_abstract;
  GIVEN("No url is set") {
    nx::WebPage web;
    REQUIRE(web.get_url () == nx::DEFAULT_URL);
    WHEN("The URL is changed"){
      web.set_url(string);
      REQUIRE(web.get_url () == string);
    }
  }
  GIVEN("The url is set at init") {
    nx::WebPage web = nx::WebPage (string);
    REQUIRE(web.get_url () == string);
    WHEN("The URL is changed"){
      web.set_url(string_2);
      REQUIRE(web.get_url () == string_2);
    }
  }
  GIVEN("The url is set at init") {
    nx::WebPage web = nx::WebPage (string);
    REQUIRE(web.get_url () == string);
    WHEN("The URL is changed"){
      web.set_url(string_2);
      REQUIRE(web.get_url () == string_2);
    }
  }
}
