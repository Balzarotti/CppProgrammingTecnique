#include <string>
#include <cstring>
#include <iostream>

#include "Document.hh"

namespace nx {
  Document::Document () {
    keywords = new std::string [0];
    keywords_number = 0;
    title = "No Name";
#ifdef DEBUG
    std::cout << "Document ()" << std::endl;
#endif
  }
  Document::~Document () {
#ifdef DEBUG
    std::cout << "~Document ()" << std::endl;
#endif

    delete [] keywords;
  }

  void Document::set_title (std::string title) {
#ifdef DEBUG
    std::cout << "Document::set_title ()" << std::endl;
#endif
    this -> title = title;
  }

  void Document::add_keyword (std::string keyword) {
#ifdef DEBUG
    std::cout << "Document::add_keyword ()" << std::endl;
#endif
    auto tmp_keywords = new std::string [++keywords_number];
    int i = 0;
    for (i = 0; i < keywords_number - 1; ++i) {
      tmp_keywords [i] = keywords[i];
    }
    tmp_keywords[i] = keyword;

    delete [] keywords;

    keywords = tmp_keywords;
  }

  void Document::print_content () {
#ifdef DEBUG
    std::cout << "Document::print_content ()" << std::endl;
#endif
    // A Document cannot have content
  }

  void Document::print() {
#ifdef DEBUG
    std::cout << "Document::print()" << std::endl;
#endif

    // print title and content only for generic "Document"
    printTitle();
    // printKeywords();
    print_content();
  }

  void Document::printTitle() {
#ifdef DEBUG
    std::cout << "Document::printTitle ()" << std::endl;
#endif
    std::cout << "-----" << title <<"------\n";
  }

  void Document::printKeywords() {
#ifdef DEBUG
    std::cout << "Document::printKeywords ()" << std::endl;
#endif
    if (keywords_number > 0) {
      std::cout << "Keywords: ";
    }
    for (int i = 0; i < keywords_number; ++i) {
      std::cout << keywords [i];
      if (i < keywords_number-1) {
	std::cout << ", ";
      }
    }
    std::cout << "\n";
  }

  WebPage::WebPage(std::string url) {
#ifdef DEBUG
    std::cout << "Document::WebPage ()" << std::endl;
#endif
    text = nullptr;
    lines = 0;
    this -> url = url;
  }

  void WebPage::print () {
#ifdef DEBUG
    std::cout << "WebPage::print()" << std::endl;
#endif

    printTitle ();
    printKeywords ();
    print_content ();
  }

  WebPage::~WebPage () {
#ifdef DEBUG
    std::cout << "~WebPage()" << std::endl;
#endif
  }

  void WebPage::add_content (std::string content) {
#ifdef DEBUG
    std::cout << "WebPage::add_content()" << std::endl;
#endif
    std::string * tmp_text = new std::string [++lines];

    int i = 0;
    for (i = 0; i < lines - 1; ++i) {
      tmp_text [i] = text [i];
    }
    tmp_text[i] = content;

    if (text != nullptr) delete [] text;

    text = tmp_text;
  }

  void WebPage::print_content() {
#ifdef DEBUG
    std::cout << "WebPage::print_content()" << std::endl;
#endif
    std::cout << "URL: " << url << "\n";

    for (int i = 0; i < lines; ++i) {
      std::cout << text [i] << "\n";
    }
  }

  std::string WebPage::get_url () {
#ifdef DEBUG
    std::cout << "WebPage::get_url()" << std::endl;
#endif
    return url;
  }
  void WebPage::set_url (std::string url) {
#ifdef DEBUG
    std::cout << "WebPage::set_url()" << std::endl;
#endif
    this -> url = url;
  }

  Spreadsheet::Spreadsheet (const int row_number,
			    const int column_number) {
#ifdef DEBUG
    std::cout << "Spreadsheet::Spreadsheet()" << std::endl;
#endif
    row_position = 0;
    column_position = 0;

    column_num = column_number;
    row_num = row_number;

    content = new double *[row_num];
    for (int i = 0; i < row_num; ++i) {
      content[i] = new double[column_num];
    }

    // Set all to 0
    for (int row = 0; row < row_num; ++row) {
      for (int col = 0; col < column_num; ++col) {
	content[row][col] = 0;
      }
    }
  }

  void Spreadsheet::print() {
#ifdef DEBUG
    std::cout << "Spreadsheet::print()" << std::endl;
#endif

    // print content only
    print_content();
  }

  Spreadsheet::~Spreadsheet () {
#ifdef DEBUG
    std::cout << "~Spreadsheet()" << std::endl;
#endif
    for (int i = 0; i < row_num; ++i) {
      delete [] content[i];
    }
    delete [] content;
  }

  void Spreadsheet::print_content () {
#ifdef DEBUG
    std::cout << "Spreadsheet::print_content()" << std::endl;
#endif
    for (int column = 0; column < column_num; ++column) {
      for (int row = 0; row < row_num; ++row) {
	std::cout << content[row][column];
	if (row+1 < row_num) std::cout << ", ";
      }
      std::cout << "\n";
    }
  }

  bool Spreadsheet::add_next_cell (const double cell_content) {
#ifdef DEBUG
    std::cout << "Spreadsheet::add_next_cell()" << std::endl;
#endif
    if (++row_position >= row_num) {
      if (++column_position >= column_num) {
	column_position = 0;
      }
      row_position = 0;
    }
    bool res = (row_position+1 == row_num && column_position+1 == column_num) ?
      false : true;
    content[row_position][column_position] = cell_content;
    return res;
  }
}
