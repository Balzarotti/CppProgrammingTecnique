#include <iostream>
#include <list>

// Example usage of the Document class + subclasses
#include "Document.hh"
int main () {
  // Check the polymorphic behaviour
  std::list<nx::Document*> Folder;

  // Spreadsheet
  nx::Spreadsheet spread = nx::Spreadsheet (10, 10);

  spread.set_title ("10x10 Spreadsheet");
  spread.add_keyword ("spreadsheet");
  spread.add_keyword ("10x10");
  spread.add_keyword ("cool");

  int counter = 0;
  while (spread.add_next_cell (++counter)) {
    // std::cout << "Adding cell value: " << counter << "\n";
  }

  // Spreadsheet
  nx::Spreadsheet spread_2 = nx::Spreadsheet (6, 2);

  spread_2.set_title ("6x2 Spreadsheet");
  spread_2.add_keyword ("spreadsheet");
  spread_2.add_keyword ("smaller");
  spread_2.add_keyword ("still-cool");

  while (spread_2.add_next_cell (static_cast<double>(counter * (counter % 3)) / 100.0)) {
    counter += 2;
    // std::cout << "Adding cell value: " << counter << "\n";
  }

  // WebPage
  nx::WebPage web;
  web.set_title ("I'm feeling ducky");
  web.add_content ("<!DOCTYPE html>");
  web.add_content ("<html>");
  web.add_content ("<body>");
  web.add_content ("");
  web.add_content ("<h1>My First Heading</h1>");
  web.add_content ("");
  web.add_content ("<p>My first paragraph.</p>");
  web.add_content ("");
  web.add_content ("</body>");
  web.add_content ("</html>");

  // WebPage
  nx::Document document;
  document.set_title("C++ Programming Techniques");
  document.add_keyword("C++");
  document.add_keyword("Programming");
  document.add_keyword("PASSED");

  Folder.push_back (&spread);
  Folder.push_back (&web);
  Folder.push_back (&spread_2);
  Folder.push_back (&document);

  for (auto doc : Folder) {
    doc -> print ();
    std::cout << "\n";
  }

}
