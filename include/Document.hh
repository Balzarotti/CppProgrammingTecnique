#include <string>
#include <iostream>
#include <ostream>

namespace nx {
  const std::string DEFAULT_URL = "https://duckduckgo.com";

  // Generic Document class (base class)
  class Document {
  private:
  public: // vars
    std::string * keywords;
    int keywords_number;
    std::string title;
  public: // Const/deconstructor
    Document();
    virtual ~Document();
    void printTitle();
    void printKeywords();
    // Document class takes care of printing the title and the keyword
    // each subclass must provide a way to print its content
    void print_content ();

  public:
    void add_keyword (std::string keyword);
    void set_title (std::string title);
    virtual void print ();
  };

  class WebPage : public Document {
  private: // vars
    std::string url;
    std::string * text;
    int lines; // = 0 // c++11
  public: // vars
    WebPage (std::string url = DEFAULT_URL);
    ~WebPage ();
  private: // methods
  public: // methods
    void print_content ();
    void add_content (std::string content);
    std::string get_url ();
    void set_url (std::string url);
    void print ();
  };

  class Spreadsheet : public Document {
  private: // vars
    int row_num;
    int column_num;
    double ** content;
    int row_position;
    int column_position;
  public: // Constructor/Deconstructor
    Spreadsheet (const int row_number = 1,
		 const int column_number = 1);
    ~Spreadsheet ();
  public: // Methods
    void print_content ();
    // false when filled the next one
    bool add_next_cell (const double cell_content);
    void print ();
  };
}
