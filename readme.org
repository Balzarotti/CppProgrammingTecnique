#+AUTHOR: Nicolò Balzarotti
#+DATE: 2017-07-04
#+TITLE: C++

How to compile:
* Install the dependencies
** If you have nix
Just write the command nix-shell to enter in a shell with everything ready
** if you do not have nix
Go installing it. Really, totally worth.
https://nixos.org/

or find out the dependencies and install them
(should depend only on meson, ninja, catch and a c++ compiler (g++/clang))
* Compiling
** Release
meson build
cd build
ninja
** Debug
meson -DDEBUG=true buildDebug
cd buildDebug
ninja
* Running
** Tests
./test/document_test
** Main
./src/main
