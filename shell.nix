with import <nixpkgs> {};
{
    Cxx = stdenv.mkDerivation {
    name = "C++";

    buildInputs = [
    # PACKAGES_PROJECTS_INSERT
      meson ninja
      catch doxygen
    ];
    # VARS_PROJECTS_INSERT

    SSL_CERT_FILE="/etc/ssl/certs/ca-certificates.crt";
    GIT_SSL_CAINFO="/etc/ssl/certs/ca-certificates.crt";
    shellHook = ''
    # START_HOOK_PROJECTS_INSERT
    unset http_proxy
    # END_HOOK_PROJECTS_INSERT
    '';
    };
}
